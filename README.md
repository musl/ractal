# ractal

A fractal image generator in rust. I write this app just about every time I learn a new programming language.

## Sample Images
![mandelbrot.png](sample-images/mandelbrot.png)
![julia.png](sample-images/julia.png)
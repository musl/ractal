use anyhow::Result;
use chrono::Utc;
use clap::Parser;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server, StatusCode};
use serde_json::json;

use std::{convert::Infallible, net::SocketAddr};

/// The command line interface for `ractal-server`.
#[derive(Clone, Debug, Parser)]
#[clap(author, about, long_about = None, version)]
pub struct Config {
    #[clap(short, long, env, default_value = "127.0.0.1:3000")]
    pub bind_addr: SocketAddr,
}

async fn handle(_: Request<Body>) -> Result<Response<Body>, Infallible> {
    let date_str = Utc::now().to_rfc3339();
    let body = Body::from(
        json!({
            "message": "Hi.",
            "date": date_str,
        })
        .to_string(),
    );
    let res = Response::builder()
        .header("X-Hix-Date", &date_str)
        .status(StatusCode::OK)
        .body(body)
        .unwrap();

    Ok(res)
}

#[tokio::main]
async fn main() -> Result<()> {
    let config = Config::parse();

    let make_svc = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });
    let server = Server::bind(&config.bind_addr).serve(make_svc);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }

    Ok(())
}

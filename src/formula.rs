use num_complex::Complex64;

#[derive(Clone, Debug, PartialEq)]
pub struct Output {
    pub i: u64,
    pub z: Complex64,
    pub power: u32,
    pub escape_distance: f64,
}

pub trait Calculate {
    fn calculate(&self, max_iter: u64, z0: Complex64) -> Output;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Formula {
    Mandelbrot {
        power: u32,
        escape_distance: f64,
    },
    Julia {
        power: u32,
        escape_distance: f64,
        c: Complex64,
    },
}

use Formula::*;

impl Calculate for Formula {
    fn calculate(&self, max_iter: u64, z0: Complex64) -> Output {
        match self {
            Mandelbrot {
                power,
                escape_distance,
            } => {
                let mut z = z0;
                let mut i = 0;

                while i < max_iter && z.norm() <= *escape_distance {
                    z = z.powi(*power as i32) + z0;
                    i += 1;
                }

                Output {
                    i,
                    z,
                    power: *power,
                    escape_distance: *escape_distance,
                }
            }
            Julia {
                power,
                escape_distance,
                c,
            } => {
                let mut z = z0;
                let mut i = 0;

                while i < max_iter && z.norm() <= *escape_distance {
                    z = z.powi(*power as i32) + c;
                    i += 1;
                }

                Output {
                    i,
                    z,
                    power: *power,
                    escape_distance: *escape_distance,
                }
            }
        }
    }
}

pub const MANDELBROT: Formula = Mandelbrot {
    power: 2,
    escape_distance: 4.0,
};

pub const JULIA: Formula = Julia {
    power: 2,
    c: Complex64 {
        re: -0.8,
        im: 0.156,
    },
    escape_distance: 4.0,
};

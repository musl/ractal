use image::Rgb;
use num_complex::Complex64;
use std::f64::consts::*;

pub mod util;
use util::UnitToU8;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Parameters {
    pub escape_distance: f64,
    pub i: u64,
    pub max_iter: u64,
    pub power: u32,
    pub x: u32,
    pub y: u32,
    pub z0: Complex64,
    pub z: Complex64,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ColorMap {
    Bands {
        member_color: Rgb<u8>,
    },
    Cantor {
        a_color: Rgb<u8>,
        b_color: Rgb<u8>,
        member_color: Rgb<u8>,
    },
    Smooth {
        member_color: Rgb<u8>,
    },
    ThreeColor {
        even_color: Rgb<u8>,
        member_color: Rgb<u8>,
        odd_color: Rgb<u8>,
    },
    ThreeColorWithBand {
        band_color: Rgb<u8>,
        band_freq: u64,
        even_color: Rgb<u8>,
        member_color: Rgb<u8>,
        odd_color: Rgb<u8>,
    },
}

use ColorMap::*;

pub trait MapColor {
    fn map_color(&self, p: Parameters) -> Rgb<u8>;
}

impl MapColor for ColorMap {
    fn map_color(&self, p: Parameters) -> Rgb<u8> {
        match self {
            Bands { member_color } => {
                let t = p.max_iter as f64 / FRAC_PI_2 * p.i as f64 / p.max_iter as f64;

                match p.i {
                    i if i == p.max_iter => *member_color,
                    _ => Rgb([
                        t.cos().unit_to_u8(),
                        (t + FRAC_PI_4).sin().unit_to_u8(),
                        t.sin().unit_to_u8(),
                    ]),
                }
            }
            Cantor {
                a_color,
                b_color,
                member_color,
            } => match p {
                p if p.i == p.max_iter => *member_color,
                _ => {
                    let q = (2.0 + 2.0 * (p.z.arg() / PI)).floor() as u64;
                    match q {
                        0 => *b_color,
                        1 => *a_color,
                        2 => *a_color,
                        _ => *b_color,
                    }
                }
            },
            Smooth { member_color } => match p.i {
                i if i == p.max_iter => *member_color,
                _ => {
                    let nu = (p.z.norm().log10() / p.escape_distance.log10()).log(p.power as f64);
                    let t = p.i as f64 + 1.0 - nu;

                    Rgb([
                        t.sin().unit_to_u8(),
                        (t + FRAC_PI_4).sin().unit_to_u8(),
                        t.cos().unit_to_u8(),
                    ])
                }
            },
            ThreeColor {
                even_color,
                odd_color,
                member_color,
            } => match p.i {
                i if i == p.max_iter => *member_color,
                i if i % 2 == 0 => *even_color,
                _ => *odd_color,
            },
            ThreeColorWithBand {
                band_color,
                band_freq,
                even_color,
                member_color,
                odd_color,
            } => match p.i {
                i if i == p.max_iter => *member_color,
                i if i % *band_freq == 0 => *band_color,
                i if i % 2 == 0 => *even_color,
                _ => *odd_color,
            },
        }
    }
}

pub const BANDS: ColorMap = Bands {
    member_color: Rgb([0, 0, 0]),
};

pub const CANTOR: ColorMap = Cantor {
    a_color: Rgb([0xff, 0xff, 0xff]),
    b_color: Rgb([0x00, 0xaa, 0xff]),
    member_color: Rgb([0, 0, 0]),
};

pub const MONO: ColorMap = ThreeColor {
    even_color: Rgb([0xff, 0xff, 0xff]),
    member_color: Rgb([0, 0, 0]),
    odd_color: Rgb([0, 0, 0]),
};

pub const MONO_WITH_BAND: ColorMap = ThreeColorWithBand {
    band_color: Rgb([0x00, 0xaa, 0xff]),
    band_freq: 10,
    even_color: Rgb([0xff, 0xff, 0xff]),
    member_color: Rgb([0, 0, 0]),
    odd_color: Rgb([0, 0, 0]),
};

pub const SMOOTH: ColorMap = Smooth {
    member_color: Rgb([0, 0, 0]),
};

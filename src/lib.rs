use image::imageops::{resize, FilterType};
use image::{GenericImage, RgbImage};
use log::info;
use num_complex::Complex64;
use rusttype::Rect;
use statrs::statistics::{Data, Distribution, Max, Min};
use std::thread::{spawn, JoinHandle};
use time::Instant;

pub mod color_map;
use color_map::{ColorMap, MapColor};

pub mod formula;
use formula::{Calculate, Formula};

/// A Rectangle on the complex plane.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct ComplexRect {
    pub max: Complex64,
    pub min: Complex64,
}

impl ComplexRect {
    pub fn width(self) -> f64 {
        self.min.re.max(self.max.re) - self.min.re.min(self.max.re)
    }

    pub fn height(self) -> f64 {
        self.min.im.max(self.max.im) - self.min.im.min(self.max.im)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Chunk {
    pub id: u32,
    pub image: RgbImage,
    pub start: Instant,
    pub y: u32,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Parameters {
    pub color_map: ColorMap,
    pub complex_rect: ComplexRect,
    pub formula: Formula,
    pub image_rect: Rect<u32>,
    pub image_scale_factor: u32,
    pub image_scale_filter: FilterType,
    pub max_iter: u64,
    pub thread_count: usize,
}

impl Parameters {
    pub fn generate(self) -> RgbImage {
        let start = Instant::now();
        let mut handles: Vec<JoinHandle<Chunk>> = vec![];

        let t = self.thread_count as u32;
        let f = self.image_scale_factor;
        let (w, h) = (self.image_rect.max.x, self.image_rect.max.y);

        let chunk_count = if h % t == 0 { t } else { t + 1 };

        let anchor_i = self.complex_rect.min.re;
        let anchor_r = self.complex_rect.min.im;
        let dx = self.complex_rect.width() / (f * w) as f64;
        let dy = self.complex_rect.height() / (f * h) as f64;

        for n in 0..chunk_count {
            let scaled_chunk_width = w * f;
            let chunk_height = if n < t { h / t } else { h % t };
            let scaled_chunk_height = chunk_height * f;
            let scaled_chunk_offset = h / t * f * n;

            handles.push(spawn(move || {
                let chunk_start = Instant::now();
                let mut chunk = RgbImage::new(scaled_chunk_width, scaled_chunk_height);

                for (x, y, pixel) in chunk.enumerate_pixels_mut() {
                    let z0 = Complex64::new(
                        anchor_r + dx * x as f64,
                        -1.0 * (anchor_i + dy * (y + scaled_chunk_offset) as f64),
                    );

                    let result = self.formula.calculate(self.max_iter, z0);
                    let params = color_map::Parameters {
                        escape_distance: result.escape_distance,
                        i: result.i,
                        max_iter: self.max_iter,
                        power: result.power,
                        x,
                        y,
                        z0,
                        z: result.z,
                    };

                    *pixel = self.color_map.map_color(params);
                }

                Chunk {
                    id: n,
                    image: resize(&chunk, w, chunk_height, self.image_scale_filter),
                    start: chunk_start,
                    y: h / t * n,
                }
            }));
        }

        // TODO: I wonder if tracing would let me have sub-targets so I don't
        // have to prepend them myself.
        info!(
            "Generate/Dispatch: {:0.6} seconds",
            start.elapsed().as_seconds_f32()
        );

        let copy_start = Instant::now();
        let mut img = RgbImage::new(w, h);
        let mut times: Vec<f64> = vec![];

        for handle in handles {
            let chunk = handle.join().unwrap();
            times.push(chunk.start.elapsed().as_seconds_f64());
            _ = img.copy_from(&chunk.image, 0, chunk.y);
        }

        let copy_elapsed = copy_start.elapsed().as_seconds_f64();
        let sum = times.iter().sum::<f64>();
        let d = Data::new(times);

        info!("Generate/Chunk-Times: {:0.6} total CPU-seconds", sum);
        info!("Generate/Chunk-Times: {:0.6} min seconds", d.min());
        info!("Generate/Chunk-Times: {:0.6} max seconds", d.max());
        info!(
            "Generate/Chunk-Times: {:0.6} mean seconds",
            d.mean().unwrap()
        );
        info!(
            "Generate/Chunk-Times: {:0.6} stddev seconds",
            d.std_dev().unwrap()
        );
        info!("Generate/Image-Copy: {:0.6} total seconds", copy_elapsed);

        img
    }
}

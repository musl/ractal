use anyhow::Result;
use env_logger::fmt::Target;
use image::imageops::FilterType;
use log::{info, Level};
use num_complex::Complex64;
use rusttype::{Point, Rect};
use time::Instant;

use ractal::color_map;
use ractal::formula;
use ractal::{ComplexRect, Parameters};

fn main() -> Result<()> {
    let app_start = Instant::now();

    env_logger::Builder::new()
        .filter_level(Level::Warn.to_level_filter())
        .target(Target::Stderr)
        .init();

    // TODO: make a better interface for making new parameters structs
    // and make serialization a thing.
    let p = Parameters {
        color_map: color_map::ColorMap::Cantor {
            member_color: image::Rgb([0, 0, 0]),
            a_color: image::Rgb([0xff, 0xff, 0xff]),
            b_color: image::Rgb([0, 0, 0]),
        },
        complex_rect: ComplexRect {
            min: Complex64::new(-6.0, -6.0),
            max: Complex64::new(6.0, 6.0),
        },
        formula: formula::JULIA,
        image_rect: Rect::<u32> {
            min: Point { x: 0, y: 0 },
            max: Point { x: 2000, y: 2000 },
        },
        image_scale_factor: 2,
        image_scale_filter: FilterType::Lanczos3,
        max_iter: 1500,
        thread_count: num_cpus::get() * 8,
    };

    let start = Instant::now();
    let img = p.generate();
    info!("Generate: {:0.3} seconds", start.elapsed().as_seconds_f32());

    let start = Instant::now();
    img.save("test.png")?;
    info!("Save: {:0.3} seconds", start.elapsed().as_seconds_f32());

    info!(
        "Total Elapsed Time: {:0.3} seconds",
        app_start.elapsed().as_seconds_f32()
    );

    Ok(())
}
